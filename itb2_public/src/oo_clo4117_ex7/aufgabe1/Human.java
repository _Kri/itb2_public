package oo_clo4117_ex7.aufgabe1;

public class Human {
	public String _name;
	public int _age;
	
	public Human(String name, int age){
		_name = name;
		_age = age;
	}
}
