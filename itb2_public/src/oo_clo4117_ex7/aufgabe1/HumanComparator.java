package oo_clo4117_ex7.aufgabe1;

import java.util.Comparator;

public class HumanComparator implements Comparator{

	@Override
	public int compare(Object o1, Object o2) {
		Human h1 = (Human)o1;
		Human h2 = (Human)o2;
		
		return h1._age - h2._age;
	}

}
