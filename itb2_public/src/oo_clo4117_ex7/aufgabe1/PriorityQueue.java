package oo_clo4117_ex7.aufgabe1;

import java.util.ArrayList;
import java.util.Comparator;

public class PriorityQueue<T>{
	Comparator<T> _comparator;
	ArrayList<T> _elements = new ArrayList<T>();
	
	public PriorityQueue(Comparator<T> comp){
		_comparator = comp;
	}
	
	public void enqueue(T element){
		_elements.add(element);
	}
	
	public T dequeue(){
		T high = _elements.get(0);
		for(int i = 1; i < _elements.size(); i++){
			if(_comparator.compare(high, _elements.get(i))>0){
				high = _elements.get(i);
			}
		}
		_elements.remove(high);
		return high;
	}
	
	public boolean isEmpty(){
		return _elements.size() == 0;
	}
	
	
	
	
	
}
