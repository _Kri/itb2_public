package oo_clo4117_ex7.aufgabe1;

import java.util.ArrayList;

public class Runner {

	public static void main(String[] args) {
		ArrayList<Human> h = new ArrayList<Human>();
		
		h.add(new Human("Egon", 24));
		h.add(new Human("Heinrich", 11));
		h.add(new Human("Ludwig", 47));
		h.add(new Human("Richard", 63));
		h.add(new Human("Friedrich", 50));
		
		HumanComparator hc = new HumanComparator();
		PriorityQueue<Human> pq = new PriorityQueue<Human>(hc);
		
		for(Human hu: h){
			pq.enqueue(hu);
		}
		
		for(int i = 0; i < h.size(); i++){
			System.out.println(pq.dequeue()._name);
		}

	}

}
