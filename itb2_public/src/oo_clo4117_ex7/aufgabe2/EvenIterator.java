package oo_clo4117_ex7.aufgabe2;

import java.util.ArrayList;
import java.util.Iterator;

public class EvenIterator implements Iterator<Integer>{
	ArrayList<Integer> _numbers = new ArrayList<Integer>();
	Integer _index = 0;

	public EvenIterator(ArrayList numbers) {
		_numbers = numbers;
	}
	
	@Override
	public boolean hasNext() {
		return getNext(false)!=null;
	}

	@Override
	public Integer next() {
		return getNext(true);
	}
	
	private Integer getNext(boolean overwrite){
		Integer index;
		if(overwrite){
			index = _index;
		}else{
			index = _index.intValue();
		}
		
		while(index < _numbers.size()){
			int num = _numbers.get(index);
			if(num%2 == 0){
				if(overwrite)_numbers.remove(index.intValue());
				return num;
			}
			index++;
		}
		return null;
	}
	
	

}
