package oo_clo4117_ex7.aufgabe2;

import java.util.ArrayList;

public class Runner {

	public static void main(String[] args) {
		ArrayList<Integer> al = new ArrayList<Integer>();
		
		al.add(1);
		al.add(2);
		al.add(55);
		al.add(6);
		al.add(3);
		al.add(4);
		al.add(27);
		al.add(342);
		
		EvenIterator ei = new EvenIterator(al);
		while(ei.hasNext()){
			System.out.println(ei.next());
		}
		
		
		
	}

}
