/**
 * Loacker Christoph
 * oo_clo4117_ex8.aufgabe2
 * 30. Mai 2017
 * OOP
 */
package oo_clo4117_ex8;

import java.io.IOException;
import java.io.InputStream;

public class CaesarInput extends InputStream{
	InputStream _in;
	int _shift;
	
	public CaesarInput(InputStream in, int shift){
		_in = in;
		_shift = shift%26;
	}

	public int read() throws IOException {
		int b = _in.read();
		
		int diffa = b-'a';
		int diffA = b-'A';
		
		//System.out.println("diff "+diffa+" "+diffA);
		
		if(diffa >= 0 && diffa < 26){
			//System.out.println("a");
			diffa-=_shift;
			diffa+=26;
			diffa=diffa%26;
			return diffa+'a';
		}else if(diffA >= 0 && diffA < 26){
			//System.out.println("A");
			diffA-=_shift;
			diffA+=26;
			diffA=diffA%26;
			return diffA+'A';
		}else{
			return b;
		}		
	}
	

}
