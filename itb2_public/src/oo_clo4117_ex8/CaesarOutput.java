/**
 * Loacker Christoph
 * oo_clo4117_ex8.aufgabe2
 * 30. Mai 2017
 * OOP
 */
package oo_clo4117_ex8;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Writer;

public class CaesarOutput extends OutputStream{
	OutputStream _out;
	int _shift = 5;

	public CaesarOutput(OutputStream out, int shift){
		_out = out;
		_shift = shift%26;
	}

	@Override
	public void write(int b) throws IOException {
		int diffa = b-'a';
		int diffA = b-'A';
		
		if(diffa >= 0 && diffa < 26){
			diffa+=_shift;
			diffa=diffa%26;
			_out.write(diffa+'a');
		}else if(diffA >= 0 && diffA < 26){
			diffA+=_shift;
			diffA=diffA%26;
			_out.write(diffA+'A');
		}else{
			_out.write(b);
		}		
	}

	public void close(){
		try {
			_out.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

}
