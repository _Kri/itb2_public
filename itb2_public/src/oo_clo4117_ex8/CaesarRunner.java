/**
 * Loacker Christoph
 * oo_clo4117_ex8.aufgabe2
 * 30. Mai 2017
 * OOP
 */
package oo_clo4117_ex8;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class CaesarRunner {

	public static void main(String[] args) {

		try {
			write();
			read();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public static void write() throws IOException {
		String outtext = "Hallo, das ist ein Test!";
		OutputStream cout = null;
		cout = new CaesarOutput(new FileOutputStream("C:/Users/chris/OneDrive - FH Vorarlberg/FHV/2. Semester/Programmieren/outputs/Test.txt"), 2);
		cout.write(outtext.getBytes());
		cout.close();

	}

	public static void read() throws IOException {
		InputStream cin;
		cin = new CaesarInput(new FileInputStream("C:/Users/chris/OneDrive - FH Vorarlberg/FHV/2. Semester/Programmieren/outputs/Test.txt"), 2);
		int b;
		StringBuilder out = new StringBuilder();
		while ((b = cin.read()) != -1) {
			out.append((char) b);
		}
		System.out.println(out.toString());

	}

}
