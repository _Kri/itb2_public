/*
 * Christoph Loacker
 * CLass: Rational
 * 09.03.2017
 */

package uebungen.uebung20170309;

public class Rational {
	private int _counter;
	private int _denom;
	
		
	public int getCounter() {
		return _counter;
	}

	public void setCounter(int counter) {
		_counter = counter;
	}

	public int getDenom() {
		return _denom;
	}

	public void setDenom(int denom) {
		_denom = denom;
	}

	public Rational(int counter, int denom){
		_counter = counter;
		_denom = denom;
	}
	
	public Rational(double value){
		
		//ToDo: In Arbeit
		/*
		String str_value = String.valueOf(value);
		int fac = (str_value.length()-str_value.indexOf("."));
		
		this._counter = 
		*/
		
		
		/*int countFac = 1;
		while(value != (int)value){
			System.out.println(value);
			value=value*10;
			countFac = countFac*10;
		}
		this._counter = (int)value;
		this._denom = countFac;
		this.shorten();
		*/
	}
	
	public void shorten(){
		if(_counter > 0){
			int ggt = Rational.ggt(_counter, _denom);
			
			_counter = (_counter/ggt);
			_denom = (_denom/ggt);
		}
		
	}
	
	public void display(){
		System.out.println(_counter+"/"+_denom);
	}
	
	public static int bringOnSameDenom(Rational r1, Rational r2){
		int factor = (r1._denom * r2._denom);
		r1._counter = (r1._counter*r2._denom);
		r2._counter = (r2._counter*r1._denom);
		r1._denom = factor;
		r2._denom = factor;
		return factor;
	}
	
	public static Rational add(Rational r1, Rational r2){
		int factor = Rational.bringOnSameDenom(r1, r2);
		Rational r = new Rational(r1._counter + r2._counter, factor);
		r1.shorten();
		r2.shorten();
		r.shorten();		
		return r;
	}
	
	public static Rational sub(Rational r1, Rational r2){
		int factor = Rational.bringOnSameDenom(r1, r2);
		Rational r = new Rational(r1._counter-r2._counter, factor);
		r1.shorten();
		r2.shorten();
		r.shorten();		
		return r;
	}
	
	private static int ggt(int num1, int num2) {
		while (num2 != 0) {
			if (num1 > num2) {
				num1 = num1 - num2;
			} else {
				num2 = num2 - num1;
			}
		}
		return num1;
	}
	
	public static void main(String args[]){
		
	}
	
	
}
