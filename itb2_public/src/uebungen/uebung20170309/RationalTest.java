/*
 * Christoph Loacker
 * 
 */
package uebungen.uebung20170309;

import static org.junit.Assert.*;

import org.junit.Test;

public class RationalTest {

	@Test
	public void test() {
		Rational r1 = new Rational(3,5);
		Rational r2 = new Rational(2,10);
		
		Rational r3 = Rational.add(r1, r2);
		assertEquals(4, r3.getCounter());
		assertEquals(5,  r3.getDenom());
		
		Rational r4 = Rational.sub(r1,  r2);
		assertEquals(2, r4.getCounter());
		assertEquals(5, r4.getDenom());
		
		Rational r5 = new Rational(0.23);
		r5.display();
		assertEquals(1, r5.getCounter());
		assertEquals(5, r5.getDenom());
	}

}
