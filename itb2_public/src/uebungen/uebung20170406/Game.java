/**
 * Loacker Christoph
 * uebung20170406
 * 06. Apr. 2017
 * OOP
 */
package uebungen.uebung20170406;

import java.util.ArrayList;

public class Game {
	ArrayList<Creature> _creatures = new ArrayList<Creature>();
	
	public void addCreature(Creature c){
		_creatures.add(c);
	}
	
	public void talk(){
		for(Creature c: _creatures){
			c.talk();
		}
	}

	
}
