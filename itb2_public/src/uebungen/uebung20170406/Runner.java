/**
 * Loacker Christoph
 * uebung20170406
 * 06. Apr. 2017
 * OOP
 */
package uebungen.uebung20170406;

public class Runner {

	public static void main(String[] args) {
		Game g = new Game();
		g.addCreature(new Donkey());
		g.addCreature(new Cow());
		g.addCreature(new Player());
		g.addCreature(new Donkey());
		g.addCreature(new Player());
		g.talk();
	}

}
