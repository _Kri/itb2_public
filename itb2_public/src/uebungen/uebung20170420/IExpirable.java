/**
 * Loacker Christoph
 * uebungen.uebung20170420
 * 20. Apr. 2017
 * OOP
 */
package uebungen.uebung20170420;

public interface IExpirable {
	public int getExpireDate();
	public void informExpired();
}
