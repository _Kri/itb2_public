/**
 * Loacker Christoph
 * uebungen.uebung20170420
 * 20. Apr. 2017
 * OOP
 */
package uebungen.uebung20170420;

public class Milk implements IExpirable{
	int _exp;

	/* (non-Javadoc)
	 * @see uebungen.uebung20170420.IExpirable#expireDate()
	 */
	@Override
	public int getExpireDate() {
		// TODO Auto-generated method stub
		return _exp;
	}

	/* (non-Javadoc)
	 * @see uebungen.uebung20170420.IExpirable#informExpired()
	 */
	@Override
	public void informExpired() {
		System.out.println("Milch: Ich bin abgelaufen " +_exp);
		
	}
	
	public Milk(int exp){
		_exp = exp;
	}

}
