/**
 * Loacker Christoph
 * uebungen.uebung20170420
 * 20. Apr. 2017
 * OOP
 */
package uebungen.uebung20170420;

import java.util.ArrayList;

public class Runner {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ArrayList<IExpirable> exp = new ArrayList<IExpirable>();
		exp.add(new Milk(15));
		exp.add(new Toast(8));
		exp.add(new Toast(12));
		exp.add(new Milk(2));
		
		for(IExpirable ex: exp){
			if(ex.getExpireDate()<10){
				ex.informExpired();
			}
		}
		

	}

}
