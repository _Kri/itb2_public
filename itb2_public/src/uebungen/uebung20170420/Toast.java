/**
 * Loacker Christoph
 * uebungen.uebung20170420
 * 20. Apr. 2017
 * OOP
 */
package uebungen.uebung20170420;

public class Toast implements IExpirable{
	int _exp;
	
	/* (non-Javadoc)
	 * @see uebungen.uebung20170420.IExpirable#getExpireDate()
	 */
	@Override
	public int getExpireDate() {
		// TODO Auto-generated method stub
		return _exp;
	}

	/* (non-Javadoc)
	 * @see uebungen.uebung20170420.IExpirable#informExpired()
	 */
	@Override
	public void informExpired() {
		System.out.println("Toast: Ich bin abgelaufen " +_exp);
		
		
	}
	
	public Toast(int exp){
		_exp = exp;
	}
 
}
