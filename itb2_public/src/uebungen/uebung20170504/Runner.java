/**
 * Loacker Christoph
 * uebungen.uebung20170504
 * 04. Mai 2017
 * OOP
 */
package uebungen.uebung20170504;

import java.util.ArrayList;
import java.util.Iterator;

public class Runner {
	
	public static void main(String[] args) {
		
		ArrayList<Integer> al = new ArrayList<Integer>();
		al.add(5);
		al.add(1);
		al.add(2);
		al.add(7);
		al.add(3);
		
		Iterator<Integer> iter = al.iterator();
		
		System.out.println("While has next");
		while(iter.hasNext()){
			Integer i = iter.next();
			System.out.println(i);
		}
		
		System.out.println("For each");
		for(Integer i: al){
			System.out.println(i);
		}
		
		
	}

}
