/**
 * Loacker Christoph
 * uebungen.uebung20170511
 * 11. Mai 2017
 * OOP
 */
package uebungen.uebung20170511;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;


public class FileMover {
	private String _filename;

	public FileMover(String filename) {
		_filename = filename;
	}

	public void write(String text) throws UnsupportedEncodingException, FileNotFoundException, IOException {
		try (Writer writer = new BufferedWriter(
				new OutputStreamWriter(new FileOutputStream(_filename), "utf-8"))) {
			writer.write(text);
		}
	}
	
	public String read() throws IOException {
		String content = null;
		File file = new File(_filename); // for ex foo.txt
		FileReader reader = null;
		try {
			reader = new FileReader(file);
			char[] chars = new char[(int) file.length()];
			reader.read(chars);
			content = new String(chars);
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (reader != null) {
				reader.close();
			}
		}
		return content;
	}
	
	public String getFilename(){
		return _filename;
	}
	
	public static void copy(FileMover from, FileMover to) throws UnsupportedEncodingException, FileNotFoundException, IOException{
		to.write(from.read());
	}

}
