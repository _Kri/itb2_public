/**
 * Loacker Christoph
 * uebungen.uebung20170511
 * 11. Mai 2017
 * OOP
 */
package uebungen.uebung20170511;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class HTTPReader {
	
	public String _url;
	
	public HTTPReader(String url){
		_url = url;
	}
	
	public String getHTML() throws Exception {
	      StringBuilder result = new StringBuilder();
	      URL url = new URL(_url);
	      HttpURLConnection conn = (HttpURLConnection) url.openConnection();
	      conn.setRequestMethod("GET");
	      BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	      String line;
	      while ((line = rd.readLine()) != null) {
	         result.append(line);
	      }
	      rd.close();
	      return result.toString();
	}
}
