/**
 * Loacker Christoph
 * uebungen.uebung20170511
 * 11. Mai 2017
 * OOP
 */
package uebungen.uebung20170511;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

public class RunnerFileMover {

	/**
	 * @param args
	 * @throws IOException
	 * @throws FileNotFoundException
	 * @throws UnsupportedEncodingException
	 */
	public static void main(String[] args) throws UnsupportedEncodingException, FileNotFoundException, IOException {
		FileMover fm = new FileMover("C:/Users/chris/Desktop/testtext.txt");
		fm.write("Hallo, ich bin ein test");
		
		System.out.println(fm.read());
		
		FileMover fm2 = new FileMover("C:/Users/chris/Desktop/testtext2.txt");
		FileMover.copy(fm, fm2);
	}

	

}
