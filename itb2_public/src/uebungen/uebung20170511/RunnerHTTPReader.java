/**
 * Loacker Christoph
 * uebungen.uebung20170511
 * 11. Mai 2017
 * OOP
 */
package uebungen.uebung20170511;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

public class RunnerHTTPReader {

	/**
	 * @param args
	 * @throws Exception 
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 * @throws UnsupportedEncodingException 
	 */
	public static void main(String[] args) throws UnsupportedEncodingException, FileNotFoundException, IOException, Exception {
		// TODO Auto-generated method stub
		FileMover fm = new FileMover("C:/Users/chris/Desktop/testtext.txt");
		HTTPReader hr = new HTTPReader("http://www.google.at");
		
		fm.write(hr.getHTML());
	}

}
