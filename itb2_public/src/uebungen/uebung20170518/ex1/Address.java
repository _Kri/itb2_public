/**
 * Loacker Christoph
 * uebungen.uebung20170518.ex1
 * 18. Mai 2017
 * OOP
 */
package uebungen.uebung20170518.ex1;

import java.io.Serializable;

public class Address implements Serializable{
	private String _address;
	
	public Address(String address){
		_address = address;
	}
	
	public String getAddress(){
		return _address;
	}
}
