/**
 * Loacker Christoph
 * uebungen.uebung20170518.ex1
 * 18. Mai 2017
 * OOP
 */
package uebungen.uebung20170518.ex1;

import java.io.Serializable;

public class Person implements Serializable{
	private String _name;
	private Address _address;
	
	public Person(String name){
		_name = name;
	}
	
	public String getName(){
		return _name;
	}
	
	public void setAddress(Address a){
		_address = a;
	}
	
	public Address getAddress(){
		return _address;
	}
	
	
}
