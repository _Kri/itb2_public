/**
 * Loacker Christoph
 * uebungen.uebung20170518.ex1
 * 18. Mai 2017
 * OOP
 */
package uebungen.uebung20170518.ex2;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class Person implements Serializable{
	private String _name;
	private transient Address _address;
	
	public Person(String name){
		_name = name;
	}
	
	public String getName(){
		return _name;
	}
	
	public void setAddress(Address a){
		_address = a;
	}
	
	public Address getAddress(){
		return _address;
	}
	
	
	private void writeObject(ObjectOutputStream oos) throws IOException{
		oos.writeObject(_name);
		oos.writeObject(_address.getAddress());
	}
	
	private void readObject(ObjectInputStream ois) throws IOException, ClassNotFoundException{
		_name = (String)ois.readObject();
		_address = new Address((String)ois.readObject());
		
	}
	
	
}
