/**
 * Loacker Christoph
 * uebungen.uebung20170518.ex1
 * 18. Mai 2017
 * OOP
 */
package uebungen.uebung20170518.ex2;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.Date;

public class Runner {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String filename = "C:/Users/chris/Desktop/outputtest.txt";
		
		Person p = new Person("Christoph");
		Address a = new Address("Zuhause");
		p.setAddress(a);
		
		
		write(filename, p);
		Person p2 = (Person)read(filename);
		System.out.println("P2 Name: "+p2.getName());
		System.out.println("P2 Address: "+p2.getAddress().getAddress());

	}

	public static void write(String filename, Person person) {
		ObjectOutputStream o = null;
		try {
			o = new ObjectOutputStream(new FileOutputStream(filename));
			o.writeObject(person);
		} catch (IOException e) {
			System.err.println(e);
		} finally {
			try {
				o.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public static Person read(String filename) {
		ObjectInputStream o = null;
		Person person = null;

		try {
			 o = new ObjectInputStream(new FileInputStream(filename));
			person = (Person) o.readObject();
		} catch (IOException e) {
			System.err.println(e);
		} catch (ClassNotFoundException e) {
			System.err.println(e);
		} finally {
			try {
				o.close();
			} catch (Exception e) {
			}
		}
		System.out.println("OK!");
		return person;
	}
	

}
