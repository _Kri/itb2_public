/**
 * Loacker Christoph
 * uebungen.uebung20170518.ex1
 * 18. Mai 2017
 * OOP
 */
package uebungen.uebung20170518.ex3;

import java.io.Serializable;
import java.util.ArrayList;

public class Person implements Serializable{
	private String _name;
	private ArrayList<Address> _address = new ArrayList<Address>();
	
	public Person(String name){
		_name = name;
	}
	
	public String getName(){
		return _name;
	}
	
	public void AddAddress(Address a){
		_address.add(a);
	}
	
	public ArrayList<Address> getAddress(){
		return _address;
	}
	
	
}
