/**
 * Loacker Christoph
 * uebungen.uebung20170518.ex4
 * 18. Mai 2017
 * OOP
 */
package uebungen.uebung20170518.ex4;

import java.awt.Frame;
import java.awt.Graphics;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Random;

public class Draw extends Frame{
	static int _height = 1000;
	static int _width = 1000;
	
	private Random _rnd = new Random();
	
	public static void main(String args[]){
		Draw d = new Draw();
		d.setVisible(true);
	}
	
	public Draw(){
		setTitle("Random Lines");
		setSize(_width, _height);  
		setLayout(null);   
		addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent e){
		      System.exit(0);
		    }        
		});
	}
	
	
	@Override
	public void paint(Graphics g) {	
		g.drawLine(_rnd.nextInt(getWidth()), _rnd.nextInt(getHeight()), _rnd.nextInt(getWidth()), _rnd.nextInt(getHeight()));
	}
	

	
	
}
