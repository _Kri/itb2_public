package uebungen.vorlesung20170316;

public class Person {
	private String _id;
	private String _name;
	private Address[] _addresses;
	private Address _mainAddress;
	
	
	public Person(String id, String name, Address mainAddress){
		_id = id;
		_name = name;
		_addresses = new Address[5];
		_addresses[0] = mainAddress;
		_mainAddress = mainAddress;
	}
}
