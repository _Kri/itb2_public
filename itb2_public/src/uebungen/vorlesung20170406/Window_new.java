/**
 * Loacker Christoph
 * vorlesung20170406
 * 06. Apr. 2017
 * OOP
 */
package uebungen.vorlesung20170406;

import java.util.LinkedList;

public class Window_new {

	
	
	private LinkedList<Geometry> _geometries = new LinkedList<Geometry>();

	public void addGeometry(Geometry geo){
		_geometries.add(geo);
	}
	
	public void move(int deltaX, int deltaY){
	
		for(Geometry g: _geometries){
			g.move(deltaX, deltaY);
		}
	}
	
	//------------------
	public static void main(String[] args) {
		Window_new w = new Window_new();
		Line l1 = new Line();
		Line l2 = new Line();
		Rect r1 = new Rect();
		Rect r2 = new Rect();
		
		w.addGeometry(l1);
		w.addGeometry(r1);
		w.addGeometry(r2);
		w.addGeometry(l2);
		
		w.move(1,2);

	}

}
