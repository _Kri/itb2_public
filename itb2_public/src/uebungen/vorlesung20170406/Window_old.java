/**
 * Loacker Christoph
 * vorlesung20170406
 * 06. Apr. 2017
 * OOP
 */
package uebungen.vorlesung20170406;

import java.util.LinkedList;

public class Window_old {

	
	
	private LinkedList<Line> _lines;
	private LinkedList<Rect> _rects;
	

	public void addLine(Line line){
		_lines.add(line);
	}
	
	public void addRect(Rect rect){
		_rects.add(rect);
	}
	
	public void move(int deltaX, int deltaY){
		for(Line l: _lines){
			l.move(deltaX, deltaY);
		}
		
		for(Rect r: _rects){
			r.move(deltaX, deltaY);
		}
	}
	
	//------------------
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
