/**
 * Loacker Christoph
 * uebungen.vorlesung20170518
 * 18. Mai 2017
 * OOP
 */
package uebungen.vorlesung20170518;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.Date;

public class Serializer {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		OutputStream fos = null;
		String filename = "C:/Users/chris/Desktop/outputtest.txt";

		try {
			fos = new FileOutputStream(filename);
			ObjectOutputStream o = new ObjectOutputStream(fos);
			o.writeObject("Today");
			o.writeObject(new Date());
		} catch (IOException e) {
			System.err.println(e);
		} finally {
			try {
				fos.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		//---------
		InputStream fis = null;

		try
		{
		  fis = new FileInputStream( filename );
		  ObjectInputStream o = new ObjectInputStream( fis );
		  String string = (String) o.readObject();
		  Date date     = (Date) o.readObject();
		  System.out.println( string );
		  System.out.println( date );
		}
		catch ( IOException e ) { System.err.println( e ); }
		catch ( ClassNotFoundException e ) { System.err.println( e ); }
		finally { try { fis.close(); } catch ( Exception e ) { } }
		System.out.println("OK!");
	}

}
